# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

long_desc = '''
This package contains the MathML Sphinx extension.

:: _MathML: http://www.w3.org/Math/
:: _Sphinx: http://sphinx.pocoo.org/

The implementation uses the blahtexml tool to do the actual conversion from
(La)TeX to MathML.

:: _blahtexml: http://gva.noekeon.org/blahtexml/

Please note that you have to set ``html_file_suffix`` to xml, or the MathML
will NOT be rendered in an otherwise capable browser.
'''

requires = ['Sphinx>=1.0']  # just to be on the caution side

setup(
    name='sphinxcontrib-mathml',
    version='0.1.1',
    url='http://bitbucket.org/coh/sphinx-contrib-mathml',
    license='BSD',
    author='Clemens-O. Hoppe',
    author_email='coh@co-hoppe.net',
    description='Sphinx "mathml" extension',
    long_description=long_desc,
    zip_safe=False,
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Console',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Documentation',
        'Topic :: Utilities',
    ],
    platforms='any',
    packages=find_packages(),
    include_package_data=True,
    install_requires=requires,
    namespace_packages=['sphinxcontrib'],
)
